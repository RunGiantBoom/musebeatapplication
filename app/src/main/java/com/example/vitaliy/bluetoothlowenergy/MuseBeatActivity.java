package com.example.vitaliy.bluetoothlowenergy;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MuseBeatActivity extends AppCompatActivity {

    BluetoothDevice device;
    Thread thread;

    int heartbpm;
    private HashMap<UUID, NotifyListener> notifyListeners = new HashMap<>();

    private TextView heart;
    private TextView text;

    private int targetbpm;
    private int curbpm;
    MusicGenerator musicGenerator = null;
    Boolean shouldContinue = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muse_beat);
//
//        ((FloatingActionButton) findViewById(R.id.connect)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                connectGatt();
//            }
//        });
//        ((FloatingActionButton) findViewById(R.id.measure)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                measure();
//            }
//        });
//        ((FloatingActionButton) findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                close();
//            }
//        });
        //Здесь нужно получить данные о девайсе к которому нужно подключиться

        heart = (TextView) findViewById(R.id.heart);
        text = (TextView) findViewById(R.id.text);

        Typeface font = Typeface.createFromAsset(getAssets(), "font/quicksandregular.ttf");
        text.setTypeface(font);
        heart.setTypeface(font);

        Intent intent = getIntent();
        device = intent.getParcelableExtra("device");

        //Сразу начинаем подключение.
        connectGatt();

        musicGenerator = new MusicGenerator(this, 1);
        //Нота задается октавой, именем , длительностью и местом в такте.
        //попробуем собирать такт.
        targetbpm = 60;
        curbpm = 60;
        int sizeOfTakt = 4;

        // Use a new tread as this can take a while
        thread = new Thread(new Runnable() {
            public void run() {

                long prevBeatTime = SystemClock.elapsedRealtime();
                long prevPulseTime = SystemClock.elapsedRealtime();
                //Здесь нужно генерить звук и воспроизводить его.
                int i = 0;
                do {

                    if (targetbpm > curbpm + 10) {
                        curbpm += 10;
                    } else if (targetbpm < curbpm - 10) {
                        curbpm -= 10;
                    } else if (targetbpm > curbpm) {
                        curbpm = targetbpm;
                    } else if (targetbpm < curbpm) {
                        curbpm = targetbpm;
                    }

                    //Мы имеем какой-то лист нот.
                    //Каждый такт в этом листе можно проиграть с разным темпом.
                    //Теперь мне нужно сказать: Сыграй пару тактов или сыграй рисунок за такое-то время
                    musicGenerator.playTakt(4 * 60000 / (float) curbpm);
                    //60000/120 = 500. То есть если прошло 500 милисекунд, то воспроизводим новый такт.
                    //Это при ритме в 120.
                    measure();
                } while (shouldContinue);
            }
        });
        thread.start();

    }

    BluetoothGatt mBluetoothGatt;

    private void connectGatt() {
        Log.d("MuseBeatActivity","Connect");
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
    }

    private final String BASIC_UUID = "0000%s-0000-1000-8000-00805f9b34fb";
    public final UUID UUID_CHARACTERISTIC_HEART_RATE_MEASUREMENT = UUID.fromString(String.format(BASIC_UUID, "2A37"));
    public final UUID UUID_DESCRIPTOR_UPDATE_NOTIFICATION = UUID.fromString(String.format(BASIC_UUID, "2902"));
    public final UUID UUID_SERVICE_HEART_RATE = UUID.fromString(String.format(BASIC_UUID, "180D"));
    public final UUID UUID_CHARACTERISTIC_HEART_RATE_CONTROL_POINT = UUID.fromString(String.format(BASIC_UUID, "2A39"));

    public final static String UUID_HEART_RATE_MEASUREMENT = "0000180d-0000-1000-8000-00805f9b34fb";
    public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";

    public BluetoothGatt gat;
    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.d("MuseBeatActivity", "Attempting to start service discovery:" +
                            mBluetoothGatt.discoverServices());
                }

                @Override
                // New services discovered
                public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                    Log.w("MuseBeatActivity", "onServicesDiscovered received: " + status);

                    gat = gatt;

                    List<BluetoothGattService> services = gatt.getServices();
                    for (BluetoothGattService bgs : services) {
                        Log.d("MuseBeatActivity", bgs.getUuid().toString());
                        if (UUID_HEART_RATE_MEASUREMENT.equals(bgs.getUuid().toString())) { //Нашли наш сервис
                            Log.d("MuseBeatActivity", "Heart is here");
                            List<BluetoothGattCharacteristic> characteristics = bgs.getCharacteristics();
                            //нашли у сервиса свойство которое позволяет считывать данные. В данном случае - измерять.
                            BluetoothGattCharacteristic characteristic = bgs.getCharacteristic(UUID_CHARACTERISTIC_HEART_RATE_MEASUREMENT); //27a9? 2A37
                            //BluetoothGattCharacteristic characteristic = characteristics.get(0);
                            gatt.setCharacteristicNotification(characteristic, true); //Сказали что нас надо будет уведомлять об изменениях

                            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_DESCRIPTOR_UPDATE_NOTIFICATION);
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            gatt.writeDescriptor(descriptor);
                            notifyListeners.put(UUID_CHARACTERISTIC_HEART_RATE_MEASUREMENT, new NotifyListener() {
                                @Override
                                public void onNotify(byte[] data) {
//        Log.d(TAG, Arrays.toString(data));
                                    final int heartRate;
                                    if (data.length == 2) {
                                        heartRate = data[1]; //Вариант для Miband2 (с первым тоже работает, однако)
//                                        if (data.length == 2 && data[0] == 6) { //Вариант для Miband1S
//                                            int heartRate = data[1] & 0xFF;
                                        heartbpm = heartRate;
                                        Log.d("Measure", Integer.toString(heartbpm));
                                    } else {
                                        heartRate = 0;
                                    }
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //targetbpm это наш целевой уровень ударов в минуту который получили при прошлом считывании
                                            //Если bpm изменился в два раза то делим или умножаем на 2. Удвоение, это косяк miband1s
                                            if (heartRate > targetbpm * 1.8) {
                                                targetbpm = heartRate / 2;
                                            } else {
                                                targetbpm = heartRate;
                                            }
                                            //Пока не будем использовать этот кусок
//                                            if (heartRate < targetbpm * 0.6) {
//                                                targetbpm = heartRate * 2;
//                                            }

                                            heart.setText(targetbpm + " bpm");
                                            text.append(targetbpm + " bpm\n",0,0);
                                            Log.d("MyActivity", "Считанный bpm: " + Integer.toString(heartRate) + ". Понято:" + targetbpm);

                                            if (heartRate == 0) {
                                                targetbpm = 1;
                                            }
                                        }
                                    });
                                }
                            });
                            //Log.d("Broadcasting", String.format("Received heart rate: %d", heartRate));
                        }
                    }
                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                    super.onCharacteristicChanged(gatt, characteristic);
                    Log.d("Characteristic", "Changed");
                    if (notifyListeners.containsKey(characteristic.getUuid())) {
                        notifyListeners.get(characteristic.getUuid()).onNotify(characteristic.getValue());
                    }
                }

                @Override
                // Result of a characteristic read operation
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    Log.d("MuseBeatActivity", characteristic.getUuid().toString());
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                    }
                }
            };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    //Скопипасчено откудато, много лишнего, но работает. Кто его знает, с какими устройствами придётся столкнуться.
    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);
        Log.d("Broadcasting", "Start");
        // This is special handling for the Heart Rate Measurement profile. Data
        // parsing is carried out as per profile specifications.
        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            Log.d("Broadcasting", "Heart is here");
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d("Broadcasting", "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d("Broadcasting", "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d("Broadcasting", String.format("Received heart rate: %d", heartRate));
            //intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
//                intent.putExtra(EXTRA_DATA, new String(data) + "\n" +
//                        stringBuilder.toString());
            }
        }
        sendBroadcast(intent);
    }



    private void measure() {
        Log.d("MuseBeatActivity", "Measure");
        byte[] protocol = new byte[]{21, 2, 1};

        BluetoothGattCharacteristic chara = gat.getService(UUID_SERVICE_HEART_RATE).getCharacteristic(UUID_CHARACTERISTIC_HEART_RATE_CONTROL_POINT);
        chara.setValue(protocol);
        if (!gat.writeCharacteristic(chara)) {
            Log.d("MuseBeatActivity", "Не записано");
        }
    }

    @Override
    public void onDestroy(){
        shouldContinue = false;
        close();
        super.onDestroy();
    }

    public void close() {
        Log.d("MuseBeatActivity", "Close");
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }




}
