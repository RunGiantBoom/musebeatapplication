package com.example.vitaliy.bluetoothlowenergy;

/**
 * Created by Vitaliy on 16.11.2017.
 */

public interface NotifyListener {
    void onNotify(byte[] data);
}