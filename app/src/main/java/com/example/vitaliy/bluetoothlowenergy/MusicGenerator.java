package com.example.vitaliy.bluetoothlowenergy;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;

import java.util.Random;

/**
 * Created by Vitaliy on 01.11.2017.
 */

class MusicGenerator {

    int tone;
    int i;
    int min;
    int max;
    int major[] = {0, 2, 4, 5, 7, 9, 11};

    private byte[] event;

    Random r;

    int soundIds[] = new int[7];
    final SoundPool sp;

    public MusicGenerator(AppCompatActivity mainActivity, int tone) {
        this.tone = tone;
        this.i = 0;

        //промежуток, в котором будем играть ноты
        min = 60;
        //max = 78;

        r = new Random();


        sp = new SoundPool(15, AudioManager.STREAM_MUSIC, 0);
        soundIds[0] = sp.load(mainActivity, R.raw.c4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[1] = sp.load(mainActivity, R.raw.d4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[2] = sp.load(mainActivity, R.raw.e4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[3] = sp.load(mainActivity, R.raw.f4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[4] = sp.load(mainActivity, R.raw.g4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[5] = sp.load(mainActivity, R.raw.a4, 1); // in 2nd param u have to pass your desire ringtone
        soundIds[6] = sp.load(mainActivity, R.raw.b4, 1); // in 2nd param u have to pass your desire ringtone

    }

    public void playTakt(float timeToPlay) { //Время которое есть на воспроизведение всего такта.
        //И его номер. На случай если мы захотим чередовать аккорды.
        //Допустим у нас есть четыре ноты

        //Мы играем либо 4 ноты, либо с промежутком.
        //Нам нужен в любом случае цикл на 4 действия.
        //Только вот рандом нотой будут не совсем рандомные ноты, а предопределённые
        //Мажорная тональность.

        int random;
        Random r = new Random();
        for (int i = 0; i < 4; i++) {
//            random = major[r.nextInt(major.length)];
//            sendMidi(0x90, min + random, 63);
            random = r.nextInt(7);
            sp.play(soundIds[random], 1, 1, 0, 0, 1);
            SystemClock.sleep((long) (timeToPlay * 0.24));
        }
    }
}
